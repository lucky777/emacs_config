# Emacs

> Emacs configuration

---

- [How to](#how-to)
  * [Install texlive-latex-base](#install-texlive-latex-base)
  * [Install pygments](#install-pygments)
  * [Install texlive-science (for algorithm2e)](#install-texlive-science--for-algorithm2e-)
  * [Install aspell (fr and en)](#install-aspell--fr-and-en-)
- [Start emacs](#start-emacs)
  * [Update elpa](#update-elpa)
  * [Install rustic](#install-rustic)
    + [Make new .org file and write '<startfile' then TAB](#make-new-org-file-and-write---startfile--then-tab)

---

# How to

## Install texlive-latex-base

    sudo apt install texlive-latex-base
    
## Install pygments

    sudo apt install python3-pygments
    
## Install texlive-science (for algorithm2e)

    sudo apt install texlive-science
    
## Install aspell (fr and en)

    sudo apt install aspell-fr aspell-en
    
# Start emacs

## Update elpa

    M+x
    
    package-install
    
    gnu-elpa-keyring-update
    
## Install rustic

    M+x
    
    package-install
    
    rustic

## Starting

    Make new .org file and write '<startfile' then TAB
